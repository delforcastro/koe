from django.contrib import admin

# Register your models here.
from concesionaria.models import Persona, CategoriaRodado, Documentacion, Rodado, DocumentacionRodado, Compra, Venta


admin.site.register(Rodado)
