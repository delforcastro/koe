from django import forms

from concesionaria.models import Persona, Documentacion, RelacionIva, CategoriaRodado, Marca, Color, Rodado, Compra, Venta, DocumentacionRodado, Gasto, TipoMotor, GastoRodado, FormaPago, EstadoCivil, Caja




class PersonaForm (forms.ModelForm):
    class Meta:
        model = Persona

        fields = [
            'denominacion',
            'domicilio',
            'provincia',
            'localidad',
            'estadoCivil',
            'telefono',
            'telefono2',
            'email',
            'nro_impositivo',
            'identidad',
            'relacionIva'
        ]

        labels = {
            'denominacion':'Denominación',
            'domicilio' : 'Domicilio',
            'provincia':'Provincia',
            'localidad':'Localidad',
            'estadoCivil' : 'Estado civil',
            'telefono':'Teléfono',
            'telefono2':'Teléfono alternativo',
            'email':'Correo electrónico',
            'nro_impositivo':'Identificación tributaria',
            'identidad':'N° Documento',
            'relacionIva':'Relación frente a IVA',
         }

        widgets = {
             'denominacion':forms.TextInput(attrs={'class':'form-control'}),
             'domicilio': forms.TextInput(attrs={'class':'form-control'}),
             'provincia': forms.TextInput(attrs={'class':'form-control'}),
             'localidad': forms.TextInput(attrs={'class':'form-control'}),
             'estadoCivil':forms.Select(),
             'telefono' : forms.TextInput(attrs={'class':'form-control'}),
             'telefono2' : forms.TextInput(attrs={'class':'form-control'}),
             'email' : forms.EmailInput(attrs={'class':'form-control'}),
             'nro_impositivo' : forms.TextInput(attrs={'class':'form-control'}),
             'identidad' : forms.TextInput(attrs={'class':'form-control'}),
             'relacionIva':forms.Select()
         }

class DocumentacionForm (forms.ModelForm):
    class Meta:
        model = Documentacion

        fields = [
            'denominacion',
            'esObligatorio'

        ]

        labels = {
            'denominacion':'Denominación',
            'esObligatorio':'Obligatorio',
         }

        widgets = {
             'denominacion':forms.TextInput(attrs={'class':'form-control'}),
             'esObligatorio':forms.CheckboxInput(),

         }


class RelacionForm (forms.ModelForm):
    class Meta:
        model = RelacionIva

        fields = [
            'denominacion',

        ]

        labels = {
            'denominacion':'Denominación',

         }

        widgets = {
             'denominacion':forms.TextInput(attrs={'class':'form-control'}),

         }


class CategoriaForm (forms.ModelForm):
    class Meta:
        model = CategoriaRodado

        fields = [
            'denominacion',

        ]

        labels = {
            'denominacion':'Denominación',

         }

        widgets = {
             'denominacion':forms.TextInput(attrs={'class':'form-control'}),

         }
class GastoForm (forms.ModelForm):
    class Meta:
        model = Gasto

        fields = [
            'denominacion',

        ]

        labels = {
            'denominacion':'Denominación',

         }

        widgets = {
             'denominacion':forms.TextInput(attrs={'class':'form-control'}),

         }


class CajaForm (forms.ModelForm):
    class Meta:
        model = Caja

        fields = [
            'denominacion',

        ]

        labels = {
            'denominacion':'Denominación',

         }

        widgets = {
             'denominacion':forms.TextInput(attrs={'class':'form-control'}),

         }

class MarcaForm (forms.ModelForm):
    class Meta:
        model = Marca

        fields = [
            'denominacion',

        ]

        labels = {
            'denominacion':'Denominación',

         }

        widgets = {
             'denominacion':forms.TextInput(attrs={'class':'form-control'}),

         }

class EstadoCivilForm (forms.ModelForm):
    class Meta:
        model = EstadoCivil

        fields = [
            'denominacion',

        ]

        labels = {
            'denominacion':'Denominación',

         }

        widgets = {
             'denominacion':forms.TextInput(attrs={'class':'form-control'}),

         }

class ColorForm (forms.ModelForm):
    class Meta:
        model = Color

        fields = [
            'denominacion',

        ]

        labels = {
            'denominacion':'Denominación',

         }

        widgets = {
             'denominacion':forms.TextInput(attrs={'class':'form-control'}),

         }

class FormaPagoForm (forms.ModelForm):
    class Meta:
        model = FormaPago

        fields = [
            'denominacion',

        ]

        labels = {
            'denominacion':'Denominación',

         }

        widgets = {
             'denominacion':forms.TextInput(attrs={'class':'form-control'}),

         }


class TipoMotorForm (forms.ModelForm):
    class Meta:
        model = TipoMotor

        fields = [
            'denominacion',

        ]

        labels = {
            'denominacion':'Denominación',

         }

        widgets = {
             'denominacion':forms.TextInput(attrs={'class':'form-control'}),

         }


class RodadoForm (forms.ModelForm):
    class Meta:
        model = Rodado

        fields = [
            'categoria',
            'marca',
            'denominacion',
            'modelo',
            'patente',
            'motor',
            'chasis',
            'color',
            'tipomotor',
            'notas',
            'imagen'
        ]

        labels = {
            'categoria':'Categoría',
            'marca' : 'Marca',
            'denominacion':'Denominación',
            'modelo':'Año',
            'patente':'Patente',
            'motor':'Motor',
            'chasis':'Chasis',
            'color':'Color',
            'tipomotor':'Tipo de motor',
            'notas':'Notas',
            'imagen':'Imagen',
         }

        widgets = {
             'categoria':forms.Select(),
             'marca': forms.Select(),
             'denominacion' : forms.TextInput(attrs={'class':'form-control'}),
             'modelo' : forms.TextInput(attrs={'class':'form-control'}),
             'patente' : forms.TextInput(attrs={'class':'form-control'}),
             'motor' : forms.TextInput(attrs={'class':'form-control'}),
             'chasis' : forms.TextInput(attrs={'class':'form-control'}),
             'color' : forms.Select(),
             'tipomotor':forms.Select(),
             'notas' : forms.TextInput(attrs={'class':'form-control'}),

         }

class CompraForm (forms.ModelForm):
    class Meta:
        model = Compra

        fields = [
            'montoCompra',
            'cotizacionDolar',
            'fechaCompra',
            'persona',
        ]

        labels = {
            'montoCompra':'Monto de compra',
            'cotizacionDolar' : 'Cotización Dólar',
            'fechaCompra':'Fecha de compra',
            'persona':'Titular',
         }

        widgets = {
             'montoCompra' : forms.TextInput(attrs={'class':'form-control'}),
             'cotizacionDolar' : forms.TextInput(attrs={'class':'form-control'}),
             'fechaCompra' : forms.DateInput(format='%Y-%m-%d',attrs={'type': 'date'}),
             'persona': forms.Select(attrs={'type':'datalist'}),

         }

class VentaForm (forms.ModelForm):
    class Meta:
        model = Venta

        fields = [
            'rodado',
            'persona',
            'montoVenta',
            'cotizacionDolar',
            'fechaVenta',
            'costoTransferencia',
        ]

        labels = {
            'rodado' : 'Rodado',
            'persona' : 'Cliente',
            'montoVenta':'Monto venta',
            'cotizacionDolar':'Cotización Dólar',
            'fechaVenta':'Fecha de venta',
            'costoTransferencia':'Costo transferencia',
         }

        widgets = {
             'rodado' : forms.Select(),
             'persona': forms.Select(),
             'montoVenta' : forms.TextInput(attrs={'class':'form-control'}),
             'cotizacionDolar' : forms.TextInput(attrs={'class':'form-control'}),
             'fechaVenta' : forms.DateInput(attrs={'type': 'date'}),
             'costoTransferencia' : forms.TextInput(attrs={'class':'form-control'}),


         }

class DocumentacionRodadoForm (forms.ModelForm):
    class Meta:
        model = DocumentacionRodado
        fields = [
            'rodado',
            'documentacion',
            'fecha',
        ]

        labels = {
            'rodado' : 'Rodado',
            'documentacion':'Documentación presentada',
            'fecha':'Fecha de presentación',
         }

        widgets = {
             'rodado' : forms.Select(),
             'documentacion': forms.Select(),
             'fecha' : forms.DateInput(attrs={'type': 'date'}),

         }

class GastoRodadoForm (forms.ModelForm):
    class Meta:
        model = GastoRodado
        fields = [
            'rodado',
            'fecha',
            'gasto',
            'montoGasto',
            'caja',
            'razonSocial',
            'numComprobante'

        ]

        labels = {
            'rodado' : 'Rodado',
            'fecha':'Fecha',
            'gasto':'Concepto de gasto',
            'montoGasto':'Monto',
            'caja':'Caja a imputar',
            'razonSocial':'Razón social',
            'numComprobante':'N° Comprobante',


         }

        widgets = {
             'rodado' : forms.Select(),
             'fecha' : forms.DateInput(format='%Y-%m-%d',attrs={'type': 'date'}),
             'gasto' : forms.Select(),
             'montoGasto' : forms.TextInput(attrs={'class':'form-control'}),
             'formaPago' : forms.Select(),
             'razonSocial' : forms.TextInput(attrs={'class':'form-control'}),
             'numComprobante': forms.TextInput(attrs={'class':'form-control'}),


         }
