from django.urls import include, path, re_path
from django.contrib.auth.decorators import login_required

from . import views
app_name = "concesionaria"

urlpatterns = [
                path('tablon', login_required(views.Tablon), name='tablon'),


                path('nuevapersona', login_required(views.NuevaPersona.as_view()), name='nueva_persona'),
                path('listapersona', login_required(views.ListaPersona.as_view()), name='lista_persona'),
                re_path(r'^editarpersona/(?P<pk>\d+)/$', login_required(views.EditarPersona.as_view()), name='editar_persona'),
                re_path(r'^eliminarpersona/(?P<pk>\d+)/$', login_required(views.EliminarPersona.as_view()), name='eliminar_persona'),

                path('nuevadocumentacion', login_required(views.NuevaDocumentacion.as_view()), name='nueva_documentacion'),
                path('listadocumentacion', login_required(views.ListaDocumentacion.as_view()), name='lista_documentacion'),
                re_path(r'^editardocumentacion/(?P<pk>\d+)/$', login_required(views.EditarDocumentacion.as_view()), name='editar_documentacion'),
                re_path(r'^eliminardocumentacion/(?P<pk>\d+)/$', login_required(views.EliminarDocumentacion.as_view()), name='eliminar_documentacion'),

                path('nuevarelacion', login_required(views.NuevaRelacion.as_view()), name='nueva_relacion'),
                path('listarelacion', login_required(views.ListaRelacion.as_view()), name='lista_relacion'),
                re_path(r'^editarrelacion/(?P<pk>\d+)/$', login_required(views.EditarRelacion.as_view()), name='editar_relacion'),
                re_path(r'^eliminarrelacion/(?P<pk>\d+)/$', login_required(views.EliminarRelacion.as_view()), name='eliminar_relacion'),

                path('nuevacategoria', login_required(views.NuevaCategoria.as_view()), name='nueva_categoria'),
                path('listacategoria', login_required(views.ListaCategoria.as_view()), name='lista_categoria'),
                re_path(r'^editarcategoria/(?P<pk>\d+)/$', login_required(views.EditarCategoria.as_view()), name='editar_categoria'),
                re_path(r'^eliminarcategoria/(?P<pk>\d+)/$', login_required(views.EliminarCategoria.as_view()), name='eliminar_categoria'),

                path('nuevamarca', login_required(views.NuevaMarca.as_view()), name='nueva_marca'),
                path('listamarca', login_required(views.ListaMarca.as_view()), name='lista_marca'),
                re_path(r'^editarmarca/(?P<pk>\d+)/$', login_required(views.EditarMarca.as_view()), name='editar_marca'),
                re_path(r'^eliminarmarca/(?P<pk>\d+)/$', login_required(views.EliminarMarca.as_view()), name='eliminar_marca'),

                path('nuevoestadocivil', login_required(views.NuevoEstadoCivil.as_view()), name='nuevo_estadocivil'),
                path('listaestadocivil', login_required(views.ListaEstadoCivil.as_view()), name='lista_estadocivil'),
                re_path(r'^editarestadocivil/(?P<pk>\d+)/$', login_required(views.EditarEstadoCivil.as_view()), name='editar_estadocivil'),
                re_path(r'^eliminarestadocivil/(?P<pk>\d+)/$', login_required(views.EliminarEstadoCivil.as_view()), name='eliminar_estadocivil'),

                path('nuevocolor', login_required(views.NuevoColor.as_view()), name='nuevo_color'),
                path('listacolor', login_required(views.ListaColor.as_view()), name='lista_color'),
                re_path(r'^editarcolor/(?P<pk>\d+)/$', login_required(views.EditarColor.as_view()), name='editar_color'),
                re_path(r'^eliminarcolor/(?P<pk>\d+)/$', login_required(views.EliminarColor.as_view()), name='eliminar_color'),

                path('nuevaformapago', login_required(views.NuevaFormaPago.as_view()), name='nueva_formapago'),
                path('listaformapago', login_required(views.ListaFormaPago.as_view()), name='lista_formapago'),
                re_path(r'^editarformapago/(?P<pk>\d+)/$', login_required(views.EditarFormaPago.as_view()), name='editar_formapago'),
                re_path(r'^eliminarformapago/(?P<pk>\d+)/$', login_required(views.EliminarFormaPago.as_view()), name='eliminar_formapago'),



                path('nuevotipomotor', login_required(views.NuevoTipoMotor.as_view()), name='nuevo_tipomotor'),
                path('listatipomotor', login_required(views.ListaTipoMotor.as_view()), name='lista_tipomotor'),
                re_path(r'^editartipomotor/(?P<pk>\d+)/$', login_required(views.EditarTipoMotor.as_view()), name='editar_tipomotor'),
                re_path(r'^eliminartipomotor/(?P<pk>\d+)/$', login_required(views.EliminarTipoMotor.as_view()), name='eliminar_tipomotor'),

                path('nuevogasto', login_required(views.NuevoGasto.as_view()), name='nuevo_gasto'),
                path('listagasto', login_required(views.ListaGasto.as_view()), name='lista_gasto'),
                re_path(r'^editargasto/(?P<pk>\d+)/$', login_required(views.EditarGasto.as_view()), name='editar_gasto'),
                re_path(r'^eliminargasto/(?P<pk>\d+)/$', login_required(views.EliminarGasto.as_view()), name='eliminar_gasto'),

                path('nuevorodado', login_required(views.NuevoRodado.as_view()), name='nuevo_rodado'),
                path('listarodado', login_required(views.ListaRodado.as_view()), name='lista_rodado'),
                re_path(r'^editarrodado/(?P<pk>\d+)/$', login_required(views.EditarRodado.as_view()), name='editar_rodado'),
                re_path(r'^eliminarrodado/(?P<pk>\d+)/$', login_required(views.EliminarRodado.as_view()), name='eliminar_rodado'),

                path('nuevaventa', login_required(views.NuevaVenta.as_view()), name='nueva_venta'),
                path('listaventa', login_required(views.ListaVenta.as_view()), name='lista_venta'),

                path('nuevadocumentacionrodado', login_required(views.NuevaDocumentacionRodado.as_view()), name='nueva_documentacionrodado'),
                path('listadocumentacionrodado', login_required(views.ListaDocumentacionRodado.as_view()), name='lista_documentacionrodado'),

                path('nuevogastorodado', login_required(views.NuevoGastoRodado.as_view()), name='nuevo_gastorodado'),
                path('listagastorodado', login_required(views.ListaGastoRodado.as_view()), name='lista_gastorodado'),
                re_path(r'^editargastorodado/(?P<id_gastorodado>\d+)/$', login_required(views.EditarGastoRodado), name='editar_gastorodado'),


                path('disponibles', login_required(views.Disponibles), name='lista_disponibles'),
                path('estadorodado/<int:id_rodado>/', login_required(views.EstadoRodado), name='estado_rodado'),

                path('confirmarboleto/<int:id_rodado>/', login_required(views.ConfirmarBoleto), name='confirmar_boleto'),

                path('nuevacaja', login_required(views.NuevaCaja.as_view()), name='nueva_caja'),
                path('listacaja', login_required(views.ListaCaja.as_view()), name='lista_caja'),
                re_path(r'^editarcaja/(?P<pk>\d+)/$', login_required(views.EditarCaja.as_view()), name='editar_caja'),

                #path('nuevacobranzatransferencia', login_required(views.NuevaCobranzaTransferencia.as_view()), name='nueva_cobranzatransferencia'),
                path('listacobranzatransferencia', login_required(views.ListaCobranzaTransferencia), name='lista_cobranzatransferencia'),
                #re_path(r'^editarcobranzatransferencia/(?P<pk>\d+)/$', login_required(views.EditarCobranzaTransferencia.as_view()), name='editar_cobranzatransferencia'),
                #re_path(r'^eliminarcobranzatransferencia/(?P<pk>\d+)/$', login_required(views.EliminarCobranzaTransferencia.as_view()), name='eliminar_cobranzatransferencia'),



]
