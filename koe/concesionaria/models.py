from django.db import models
from django.contrib.auth.models import User
# Create your models here.
#LISTO EL CRUD
class RelacionIva (models.Model):
    denominacion = models.CharField(max_length=200)
    def __str__(self):
        return self.denominacion

class EstadoCivil (models.Model):

    denominacion = models.CharField(max_length=150)

    def __str__(self):
        return self.denominacion

class Persona (models.Model):
    #Esta clase representa a las personas, pueden ser vendedores o compradores
    #LISTO EL CRUD
    denominacion = models.CharField(max_length=200)
    domicilio = models.CharField(max_length=150, null=True, blank=True)
    telefono = models.CharField(max_length=40, blank=True)
    telefono2 = models.CharField(max_length=40, blank=True)
    estadoCivil = models.ForeignKey(EstadoCivil, on_delete=models.SET_NULL, null=True, blank=True)
    email = models.EmailField(max_length=150)
    provincia = models.CharField(max_length=100, blank=True, null=True)
    localidad = models.CharField(max_length=150, blank=True, null=True)
    #nro_impositivo está pensado para almacenar un número
    #de identificación tributaria, como CUIT en Argentina.
    nro_impositivo = models.CharField(max_length=20, null=True, blank=True)

    identidad = models.IntegerField(unique=True, blank=True, null=True) #DNI ÚNICO
    creado = models.DateTimeField(auto_now_add=True)
    relacionIva = models.ForeignKey(RelacionIva,on_delete=models.SET_NULL, null=True)

    fecha_record = models.DateField(auto_now_add=True, blank=True, null=True)
    user_record = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.denominacion


class CategoriaRodado (models.Model):
    #LISTO EL CRUD
    denominacion = models.CharField(max_length=70)

    def __str__(self):
        return self.denominacion

class Documentacion (models.Model):
    #LISTO EL CRUD
    denominacion = models.CharField(max_length=150)
    esObligatorio = models.BooleanField(null=True, blank=True)
    def __str__(self):
        return self.denominacion

class Marca (models.Model):
    #LISTO EL CRUD
    denominacion = models.CharField(max_length=150)

    def __str__(self):
        return self.denominacion



class TipoMotor (models.Model):
    denominacion = models.CharField(max_length=100)

    def __str__(self):
        return self.denominacion

class FormaPago (models.Model):
    denominacion = models.CharField(max_length=100)

    def __str__(self):
        return self.denominacion

class Color (models.Model):
    #LISTO EL CRUD
    denominacion = models.CharField(max_length=150)

    def __str__(self):
        return self.denominacion


class Gasto (models.Model):
    denominacion = models.CharField(max_length=150)

    def __str__(self):
        return self.denominacion


class Caja (models.Model):
    denominacion = models.CharField(max_length=150)

    def __str__(self):
        return self.denominacion


class Rodado (models.Model):
    #Esta clase representa a los rodados que se comercializan
    categoria = models.ForeignKey(CategoriaRodado, on_delete=models.SET_NULL, null=True)
    denominacion = models.CharField(max_length=200)
    modelo = models.IntegerField(blank=True, null=True)
    notas = models.CharField (max_length=200, blank=True, null=True)
    patente = models.CharField(max_length=20)
    motor = models.CharField(max_length=100, blank=True, null=True)
    chasis = models.CharField(max_length=100, blank=True, null=True)
    marca = models.ForeignKey(Marca,on_delete=models.SET_NULL, null=True)
    color = models.ForeignKey(Color,on_delete=models.SET_NULL, null=True)
    tipomotor = models.ForeignKey(TipoMotor, on_delete=models.SET_NULL, null=True, blank=True)
    imagen = models.ImageField(null=True, blank=True, upload_to="images/")
    fecha_record = models.DateField(auto_now_add=True, blank=True, null=True)
    user_record = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)

    #status puede ser: 1= en stock, 2=vendido
    status = models.IntegerField(default=1)

    def __str__(self):
        return self.marca.denominacion + " " + self.denominacion + " | Año: " + str(self.modelo) + " | Patente: " + self.patente


class DocumentacionRodado (models.Model):
    #Esta clase representa la entrega de documentación del rodado
    rodado = models.ForeignKey(Rodado, on_delete=models.CASCADE)
    fecha = models.DateTimeField(blank=True, null=True)
    documentacion = models.ForeignKey(Documentacion, on_delete=models.CASCADE)


class GastoRodado (models.Model):
    rodado = models.ForeignKey(Rodado, on_delete=models.CASCADE)
    fecha = models.DateTimeField(blank=True, null=True)
    gasto = models.ForeignKey(Gasto, on_delete=models.CASCADE)
    montoGasto = models.FloatField()
    caja = models.ForeignKey(Caja, on_delete=models.SET_NULL, null=True, blank=True)
    numComprobante = models.CharField(max_length=100, blank=True, null=True)
    razonSocial = models.CharField(max_length=200, blank=True, null=True)
    fecha_record = models.DateField(auto_now_add=True, blank=True, null=True)
    user_record = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)



class Compra (models.Model):
    #Este modelo representa los datos de la compra de un rodado
    montoCompra = models.FloatField(blank=True, null=True)
    cotizacionDolar = models.FloatField(blank=True, null=True)
    fechaCompra = models.DateTimeField(blank=True, null=True)
    persona = models.ForeignKey(Persona, on_delete=models.CASCADE)
    rodado=models.ForeignKey(Rodado, on_delete=models.CASCADE)

    fecha_record = models.DateField(auto_now_add=True, blank=True, null=True)
    user_record = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)


class Venta (models.Model):
    #Este modelo representa los datos generales de venta de un rodado
    #VER modelo financiacion
    montoVenta = models.FloatField(blank=True, null=True)
    cotizacionDolar = models.FloatField(blank=True, null=True)
    fechaVenta = models.DateTimeField(blank=True, null=True)
    persona = models.ForeignKey(Persona, on_delete=models.CASCADE)
    rodado = models.ForeignKey(Rodado, on_delete=models.CASCADE)
    costoTransferencia = models.FloatField(blank=True, null=True)
    fecha_record = models.DateField(auto_now_add=True, blank=True, null=True)
    user_record = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)

class CobranzaTransferencia(models.Model):
    venta = models.ForeignKey(Venta, on_delete=models.SET_NULL, null=True, blank=True)
    fechaCobranza = models.DateTimeField(blank=True, null=True)
    fecha_record = models.DateField(auto_now_add=True, blank=True, null=True)
    user_record = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)


class Transferencia (models.Model):
    fechaTransferencia = models.DateTimeField()
    rodado = models.ForeignKey(Rodado, on_delete=models.CASCADE)

    fecha_record = models.DateField(auto_now_add=True, blank=True, null=True)
    user_record = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
