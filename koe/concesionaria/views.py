from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.views.generic import ListView, CreateView, UpdateView, DeleteView, TemplateView
from django.urls import reverse_lazy
from django.db.models import Sum
# Vistas de la aplicación 'terminal'
from concesionaria.models import Persona, Documentacion, RelacionIva, CategoriaRodado, Marca, Color, Rodado, Compra, Venta, DocumentacionRodado, Gasto, TipoMotor, GastoRodado, FormaPago, EstadoCivil, Caja, Transferencia
from concesionaria.forms import PersonaForm, DocumentacionForm, RelacionForm, CategoriaForm, MarcaForm, ColorForm, RodadoForm, CompraForm, VentaForm, DocumentacionRodadoForm, GastoForm, TipoMotorForm, GastoRodadoForm, FormaPagoForm, EstadoCivilForm, CajaForm

#Vistas para CRUD Terminal


def Tablon(request):
	personas = Persona.objects.all()


	contexto = {'personas': personas}

	return render(request, 'tablon/tablon.html', contexto)

def Disponibles(request):
	vendidos = Venta.objects.values_list('rodado_id')
	disponibles = Compra.objects.exclude(id__in=vendidos)
	context = {'disponibles' : disponibles}

	return render(request, 'concesionaria/lista/disponibles.html', context)

def ListaCobranzaTransferencia(request):
	#obtengo lista de los transferidos
	transferidos = Transferencia.objects.values_list('rodado_id')
	#obtengo lista de pendientes
	pendientes = Venta.objects.exclude(id__in=transferidos)
	context ={'pendientes': pendientes}

	return render(request, 'concesionaria/lista/cobranzatransferencia.html', context)


def EstadoRodado (request, id_rodado):
	rodado = Rodado.objects.get(id=id_rodado)
	#documentacion = Documentacion.objects.all()

	documentacionrodado = DocumentacionRodado.objects.filter(rodado=id_rodado)
	documentacionpendiente = Documentacion.objects.exclude(id__in=documentacionrodado)
	gastosrodado = GastoRodado.objects.filter(rodado=id_rodado)
	total = GastoRodado.objects.filter(rodado=id_rodado).aggregate(total=Sum('montoGasto'))['total']
	compra = Compra.objects.get(rodado=id_rodado)

	context = {'rodado':rodado,
				'documentacionrodado':documentacionrodado,
				'documentacionpendiente':documentacionpendiente,
				'gastos':gastosrodado,
				'compra':compra,
				'total':total}
	return render(request, 'concesionaria/estado/rodado.html', context)

#Vistas para CRUD Persona
class NuevaPersona (CreateView):
	model = Persona
	form_class = PersonaForm
	template_name = 'concesionaria/nueva/persona.html'
	success_url = reverse_lazy('concesionaria:lista_persona')

class ListaPersona (ListView):
	model = Persona
	template_name = 'concesionaria/lista/persona.html'

class EditarPersona (UpdateView):
	model = Persona
	form_class = PersonaForm
	template_name = 'concesionaria/nueva/persona.html'
	success_url = reverse_lazy('concesionaria:lista_persona')

class EliminarPersona (DeleteView):
	model = Persona
	template_name = 'concesionaria/eliminar/persona.html'
	success_url = reverse_lazy('concesionaria:lista_persona')

#Vistas para CRUD Documento
class NuevaDocumentacion (CreateView):
	model = Documentacion
	form_class = DocumentacionForm
	template_name = 'concesionaria/nueva/documentacion.html'
	success_url = reverse_lazy('concesionaria:tablon')

class ListaDocumentacion (ListView):
	model = Documentacion
	template_name = 'concesionaria/lista/documentacion.html'

class EditarDocumentacion (UpdateView):
	model = Documentacion
	form_class = DocumentacionForm
	template_name = 'concesionaria/nueva/documentacion.html'
	success_url = reverse_lazy('concesionaria:tablon')

class EliminarDocumentacion (DeleteView):
	model = Documentacion
	template_name = 'concesionaria/eliminar/documentacion.html'
	success_url = reverse_lazy('concesionaria:tablon')

#Vistas para CRUD Gasto
class NuevoGasto (CreateView):
	model = Gasto
	form_class = GastoForm
	template_name = 'concesionaria/nueva/gasto.html'
	success_url = reverse_lazy('concesionaria:lista_gasto')

class ListaGasto (ListView):
	model = Gasto
	template_name = 'concesionaria/lista/gasto.html'

class EditarGasto (UpdateView):
	model = Gasto
	form_class = GastoForm
	template_name = 'concesionaria/nueva/gasto.html'
	success_url = reverse_lazy('concesionaria:lista_gasto')

class EliminarGasto (DeleteView):
	model = Gasto
	template_name = 'concesionaria/eliminar/gasto.html'
	success_url = reverse_lazy('concesionaria:lista_gasto')

#Vistas para CRUD Caja
class NuevaCaja (CreateView):
	model = Caja
	form_class = CajaForm
	template_name = 'concesionaria/nueva/caja.html'
	success_url = reverse_lazy('concesionaria:lista_caja')

class ListaCaja (ListView):
	model = Caja
	template_name = 'concesionaria/lista/caja.html'

class EditarCaja (UpdateView):
	model = Caja
	form_class = CajaForm
	template_name = 'concesionaria/nueva/caja.html'
	success_url = reverse_lazy('concesionaria:lista_caja')

#Vistas para CRUD tipo Motor
class NuevoTipoMotor (CreateView):
	model = TipoMotor
	form_class = TipoMotorForm
	template_name = 'concesionaria/nueva/tipomotor.html'
	success_url = reverse_lazy('concesionaria:lista_tipomotor')

class ListaTipoMotor (ListView):
	model = TipoMotor
	template_name = 'concesionaria/lista/tipomotor.html'

class EditarTipoMotor (UpdateView):
	model = TipoMotor
	form_class = TipoMotorForm
	template_name = 'concesionaria/nueva/tipomotor.html'
	success_url = reverse_lazy('concesionaria:lista_tipomotor')

class EliminarTipoMotor (DeleteView):
	model = TipoMotor
	template_name = 'concesionaria/eliminar/tipomotor.html'
	success_url = reverse_lazy('concesionaria:lista_tipomotor')

#Vistas para CRUD Relacion frente a IVA
class NuevaRelacion (CreateView):
	model = RelacionIva
	form_class = RelacionForm
	template_name = 'concesionaria/nueva/relacion.html'
	success_url = reverse_lazy('concesionaria:tablon')

class ListaRelacion (ListView):
	model = RelacionIva
	template_name = 'concesionaria/lista/relacion.html'

class EditarRelacion (UpdateView):
	model = RelacionIva
	form_class = RelacionForm
	template_name = 'concesionaria/nueva/relacion.html'
	success_url = reverse_lazy('concesionaria:tablon')

class EliminarRelacion (DeleteView):
	model = RelacionIva
	template_name = 'concesionaria/eliminar/relacion.html'
	success_url = reverse_lazy('concesionaria:tablon')

#Vistas para CRUD Categorías Rodados
class NuevaCategoria (CreateView):
	model = CategoriaRodado
	form_class = CategoriaForm
	template_name = 'concesionaria/nueva/categoria.html'
	success_url = reverse_lazy('concesionaria:tablon')

class ListaCategoria (ListView):
	model = CategoriaRodado
	template_name = 'concesionaria/lista/categoria.html'

class EditarCategoria (UpdateView):
	model = CategoriaRodado
	form_class = CategoriaForm
	template_name = 'concesionaria/nueva/categoria.html'
	success_url = reverse_lazy('concesionaria:tablon')

class EliminarCategoria (DeleteView):
	model = CategoriaRodado
	template_name = 'concesionaria/eliminar/categoria.html'
	success_url = reverse_lazy('concesionaria:tablon')

#Vistas para CRUD Marcas
class NuevaMarca (CreateView):
	model = Marca
	form_class = MarcaForm
	template_name = 'concesionaria/nueva/marca.html'
	success_url = reverse_lazy('concesionaria:tablon')

class ListaMarca (ListView):
	model = Marca
	template_name = 'concesionaria/lista/marca.html'

class EditarMarca (UpdateView):
	model = Marca
	form_class = MarcaForm
	template_name = 'concesionaria/nueva/marca.html'
	success_url = reverse_lazy('concesionaria:tablon')

class EliminarMarca (DeleteView):
	model = Marca
	template_name = 'concesionaria/eliminar/marca.html'
	success_url = reverse_lazy('concesionaria:tablon')


#Vistas para CRUD Estado civil
class NuevoEstadoCivil (CreateView):
	model = EstadoCivil
	form_class = EstadoCivilForm
	template_name = 'concesionaria/nueva/estadocivil.html'
	success_url = reverse_lazy('concesionaria:tablon')

class ListaEstadoCivil (ListView):
	model = EstadoCivil
	template_name = 'concesionaria/lista/estadocivil.html'

class EditarEstadoCivil (UpdateView):
	model = EstadoCivil
	form_class = EstadoCivilForm
	template_name = 'concesionaria/nueva/estadocivil.html'
	success_url = reverse_lazy('concesionaria:tablon')

class EliminarEstadoCivil (DeleteView):
	model = EstadoCivil
	template_name = 'concesionaria/eliminar/estadocivil.html'
	success_url = reverse_lazy('concesionaria:tablon')


#Vistas para CRUD Color
class NuevoColor (CreateView):
	model = Color
	form_class = ColorForm
	template_name = 'concesionaria/nueva/color.html'
	success_url = reverse_lazy('concesionaria:tablon')

class ListaColor (ListView):
	model = Color
	template_name = 'concesionaria/lista/color.html'

class EditarColor (UpdateView):
	model = Color
	form_class = ColorForm
	template_name = 'concesionaria/nueva/color.html'
	success_url = reverse_lazy('concesionaria:tablon')

class EliminarColor (DeleteView):
	model = Color
	template_name = 'concesionaria/eliminar/color.html'
	success_url = reverse_lazy('concesionaria:tablon')


#Vistas para CRUD Formas de pago
class NuevaFormaPago (CreateView):
	model = FormaPago
	form_class = FormaPagoForm
	template_name = 'concesionaria/nueva/formapago.html'
	success_url = reverse_lazy('concesionaria:lista_formapago')

class ListaFormaPago (ListView):
	model = FormaPago
	template_name = 'concesionaria/lista/formapago.html'

class EditarFormaPago (UpdateView):
	model = FormaPago
	form_class = FormaPagoForm
	template_name = 'concesionaria/nueva/formapago.html'
	success_url = reverse_lazy('concesionaria:lista_formapago')

class EliminarFormaPago (DeleteView):
	model = FormaPago
	template_name = 'concesionaria/eliminar/formapago.html'
	success_url = reverse_lazy('concesionaria:lista_formapago')



#Vistas para CRUD Rodado
class NuevoRodado (CreateView):
	model = Compra
	form_class = CompraForm
	second_form_class = RodadoForm
	template_name = 'concesionaria/nueva/rodado.html'
	success_url = reverse_lazy('concesionaria:lista_rodado')

	def get_context_data(self, **kwargs):
		context = super(NuevoRodado, self).get_context_data(**kwargs)
		if 'form' not in context:
			context['form'] = self.form_class(self.request.GET)
		if 'form2' not in context:
			context['form2'] = self.second_form_class(self.request.GET)
		return context
	def post (self, request, *args, **kwargs):
		self.object = self.get_object
		form = self.form_class(request.POST)
		form2 = self.second_form_class(request.POST)

		if form.is_valid() and form2.is_valid():
			compra = form.save(commit=False)
			compra.rodado = form2.save(commit=False)
			compra.rodado.user_record = request.user
			compra.rodado = form2.save()
			compra.save()

			return HttpResponseRedirect(self.get_success_url())
		else:
			return self.render_to_response (self.get_context_data(form=form, form2=form2))





class ListaRodado (ListView):
	model = Rodado
	template_name = 'concesionaria/lista/rodado.html'

class EditarRodado (UpdateView):
	model = Compra
	second_model = Rodado
	template_name = 'concesionaria/nueva/rodado.html'
	form_class = CompraForm
	second_form_class = RodadoForm
	success_url = reverse_lazy('concesionaria:lista_rodado')
	def get_context_data(self, **kwargs):
		context = super(EditarRodado, self).get_context_data(**kwargs)
		pk = self.kwargs.get('pk',0)
		compra = self.model.objects.get(id=pk)
		rodado = self.second_model.objects.get(id=compra.rodado_id)
		url_img = rodado.imagen
		if 'form' not in context:
			context['form'] = self.form_class()
		if 'form2' not in context:
			context['form2'] = self.second_form_class(instance=rodado)
			context['id'] = pk
			context['url_img'] = url_img
		return context

	def post(self, request, *args, **kwargs):
		self.object = self.get_object
		id_compra = kwargs['pk']
		compra = self.model.objects.get(id=id_compra)
		rodado = self.second_model.objects.get(id=compra.rodado_id)
		form = self.form_class(request.POST, instance=compra) #si no creo una instancia guarda un nuevo objeto
		form2 = self.second_form_class(request.POST, request.FILES, instance=rodado)

		if form.is_valid() and form2.is_valid():
			form.save()
			form2.save()
			return HttpResponseRedirect(self.get_success_url())
		else:
			return HttpResponseRedirect(self.get_success_url())


class EliminarRodado (DeleteView):
	model = Rodado
	template_name = 'concesionaria/eliminar/rodado.html'
	success_url = reverse_lazy('concesionaria:lista_rodado')



#CRUD VENTAS
class NuevaVenta (CreateView):
	model = Venta
	form_class = VentaForm
	template_name = 'concesionaria/nueva/venta.html'
	success_url = reverse_lazy('concesionaria:tablon')

	def get_context_data(self, **kwargs):
		vendidos = Venta.objects.values_list('rodado_id')
		disponibles = Rodado.objects.exclude(id__in=vendidos)
		context = {'disponibles' : disponibles}

		if 'form' not in context:
			context['form'] = self.form_class(self.request.GET)
		return context



class ListaVenta (ListView):
	model = Venta
	template_name = 'concesionaria/lista/venta.html'


class NuevaDocumentacionRodado (CreateView):
	model = DocumentacionRodado
	form_class = DocumentacionRodadoForm
	template_name = 'concesionaria/nueva/documentacionrodado.html'
	success_url = reverse_lazy('concesionaria:lista_documentacionrodado')

	def get_context_data(self, **kwargs):
		vendidos = Venta.objects.values_list('rodado_id')
		disponibles = Rodado.objects.exclude(id__in=vendidos)
		context = {'disponibles' : disponibles}

		if 'form' not in context:
			context['form'] = self.form_class(self.request.GET)
		return context

class ListaDocumentacionRodado (ListView):
	model = DocumentacionRodado
	template_name = 'concesionaria/lista/documentacionrodado.html'


class NuevoGastoRodado (CreateView):
	model = GastoRodado
	form_class = GastoRodadoForm
	template_name = 'concesionaria/nueva/gastorodado.html'
	success_url = reverse_lazy('concesionaria:lista_gastorodado')

	def get_context_data(self, **kwargs):
		vendidos = Venta.objects.values_list('rodado_id')
		disponibles = Rodado.objects.exclude(id__in=vendidos)
		context = {'disponibles' : disponibles}

		if 'form' not in context:
			context['form'] = self.form_class(self.request.GET)
		return context

class ListaGastoRodado (ListView):
	model = GastoRodado
	template_name = 'concesionaria/lista/gastorodado.html'


def EditarGastoRodado(request, id_gastorodado):
	gastorodado = GastoRodado.objects.get(id=id_gastorodado)
	success_url = reverse_lazy('concesionaria:lista_gastorodado')
	if request.method == 'GET':
		form = GastoRodadoForm(instance=gastorodado)
	else:
		form = GastoRodadoForm(request.POST, instance=gastorodado)

		if form.is_valid():
			form.save()
		return redirect('concesionaria:lista_gastorodado')
	vendidos = Venta.objects.values_list('rodado_id')
	disponibles = Rodado.objects.exclude(id__in=vendidos)

	return render(request, 'concesionaria/nueva/gastorodado.html', {'form':form, 'disponibles': disponibles, 'gasto': gastorodado})


def ConfirmarBoleto (request, id_rodado):

		rodado = Rodado.objects.get(id=id_rodado)
		compra = Compra.objects.get(rodado=rodado.id)
		persona = Persona.objects.get(id=compra.persona.id)
		venta = Venta.objects.get(rodado=rodado.id)
		context = {'rodado' : rodado, 'compra' : compra, 'persona': persona, 'venta' : venta}
		if request.method =='GET':
			return render (request, 'concesionaria/reporte/confirmarboleto.html', context)
		elif request.method =='POST':
			vendor = request.POST['vendedor']
			if vendor != '-1':
				return render (request, 'concesionaria/reporte/boletoCompraVenta.html', context)
			else:
				return render(request, 'concesionaria/reporte/boletoCompraVenta2.html')
