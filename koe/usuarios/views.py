from django.shortcuts import render

from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.views.generic import CreateView
from django.urls import reverse_lazy
from usuarios.forms import RegistroUsuarioForm

class RegistroUsuario (CreateView):
    model = User
    template_name = 'usuario/registro.html'
    form_class = RegistroUsuarioForm
    success_url = reverse_lazy('concesionaria:tablon') #Esto hay que cambiarlo
